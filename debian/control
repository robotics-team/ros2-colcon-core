Source: ros2-colcon-core
Section: python
Priority: optional
Maintainer: Debian Robotics Team <team+robotics@tracker.debian.org>
Uploaders:
 Timo Röhling <roehling@debian.org>,
 Jochen Sprickerhof <jspricke@debian.org>,
 Timon Engelke <debian@timonengelke.de>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pylint <!nocheck>,
 python3-all,
 python3-distlib,
 python3-empy,
 python3-pep8-naming <!nocheck>,
 python3-pydocstyle <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-setuptools,
Standards-Version: 4.7.0
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no
Homepage: https://colcon.readthedocs.io/
Vcs-Git: https://salsa.debian.org/robotics-team/ros2-colcon-core.git
Vcs-Browser: https://salsa.debian.org/robotics-team/ros2-colcon-core
Description: collective construction meta build tool
 This package is part of ROS 2, the Robot Operating System.
 colcon is a meta build tool to improve the workflow of building, testing and
 using multiple software packages. It is the recommended tool for ROS 2 to set
 up workspaces and build packages from source, but can also handle old ROS 1
 workspaces.

Package: colcon
Section: devel
Architecture: all
Multi-Arch: foreign
Depends:
 python3-colcon-core,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-colcon-argcomplete,
 python3-colcon-bash,
 python3-colcon-cd,
 python3-colcon-cmake,
 python3-colcon-defaults,
 python3-colcon-devtools,
 python3-colcon-library-path,
 python3-colcon-metadata,
 python3-colcon-notification,
 python3-colcon-output,
 python3-colcon-package-information,
 python3-colcon-package-selection,
 python3-colcon-parallel-executor,
 python3-colcon-python-setup-py,
 python3-colcon-recursive-crawl,
 python3-colcon-ros,
 python3-colcon-test-result,
 python3-colcon-zsh,
Description: ${source:Synopsis}
 ${source:Extended-Description}
 .
 This package installs the main colcon executable.

Package: python3-colcon-core
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 colcon,
Suggests:
 python3-pytest,
 python3-pytest-cov,
Description: ${source:Synopsis} - core library
 ${source:Extended-Description}
 .
 This package installs the core library for Python 3.
